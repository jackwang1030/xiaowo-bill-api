export default () => ({
  redis: {
    namespace: process.env.REDIS_NAME_SPACE || 'default',
    port: parseInt(process.env.REDIS_PORT, 10) || 6379,
    db: parseInt(process.env.REDIS_DB, 10) || 0,
    host: process.env.REDIS_HOST || '127.0.0.1',
    password: process.env.REDIS_PWD || '',
  },
  db: {
    port: parseInt(process.env.DB_PORT, 10) || 3306,
    host: process.env.DB_HOST || '127.0.0.1',
    user: process.env.DB_USER || 'root',
    password: process.env.DB_PASSWD || '123456',
    database: process.env.DB_DATABASE || 'xiaowo-bill-dev',
  },
  jwt: {
    secret: process.env.SECRET || 'test123456',
  },
  token_expire: {
    access_token_expire: process.env.ACCESS_TOKEN_EXPIRE || 86400,
    refresh_token_expire: process.env.REFRESH_TOKEN_EXPIRE || 604800,
  },
});
