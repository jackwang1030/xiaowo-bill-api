// import * as fs from 'fs';
import * as path from 'path';
import { Logger } from 'src/utils/log4js';
const isProd = process.env.NODE_ENV === 'production';

function parseEnv() {
  const localEnv = path.resolve('development.env');
  const prodEnv = path.resolve('production.env');

  // if (!fs.existsSync(localEnv) && !fs.existsSync(prodEnv)) {
  //   throw new Error('缺少环境配置文件');
  // }
  Logger.info(`env: ${process.env.NODE_ENV}`);
  const filePath = isProd ? prodEnv : localEnv;
  return { path: filePath };
}

export default parseEnv();
