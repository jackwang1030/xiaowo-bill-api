const productConfig = {
  mysql: {
    host: '127.0.0.1',
    port: 3306,
    user: 'root',
    password: '@bdmysqlpwd%',
    database: 'xiaowo-bill-dev',
  },
};

const localConfig = {
  mysql: {
    host: 'localhost',
    port: 3306,
    user: 'root',
    password: '123456',
    database: 'xiaowo-bill-dev',
  },
};

// 本地运行是没有 process.env.NODE_ENV 的，借此来区分[开发环境]和[生产环境]
const config = process.env.NODE_ENV ? productConfig : localConfig;

export default config;
