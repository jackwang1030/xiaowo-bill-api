import { Logger } from './utils/log4js';
import { Sequelize } from 'sequelize-typescript';
import config from '../config/db';

const {
  mysql: { port, host, user, password, database },
} = config;

const sequelize = new Sequelize(database, user, password, {
  dialect: 'mysql',
  host: host,
  port: port,
  username: user,
  password: password,
  database: database,
  timezone: '+08:00',
  dialectOptions: {
    dateStrings: true,
    typeCast: true,
  },
});

sequelize
  .authenticate()
  .then(() => {
    Logger.info('数据库连接成功');
  })
  .catch((err: any) => {
    // 数据库连接失败时打印输出
    Logger.error('数据库连接失败' + err);
    throw err;
  });

export default sequelize;
