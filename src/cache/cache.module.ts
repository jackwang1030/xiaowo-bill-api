import { Module } from '@nestjs/common';
import { RedisModule } from '@chenjm/nestjs-redis';
import { CacheService } from './cache.service';
import { ConfigService, ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    RedisModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => {
        return {
          closeClient: true,
          readyLog: true,
          config: configService.get('redis'),
        };
        // port: configService.get('REDIS_PORT'),
        // host: configService.get('REDIS_HOST'),
        // password: configService.get('REDIS_PASSWD'),
        // decode_responses: true,
        // db: configService.get('REDIS_DB'),
      },
      inject: [ConfigService],
    }),
  ],

  providers: [CacheService],

  exports: [CacheService],
})
export class CacheModule {}
