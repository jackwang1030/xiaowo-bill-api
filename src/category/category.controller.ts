import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { Public } from 'src/auth/auth.decorator';
import { CategoryService } from './category.service';
import { CategoryQueryDto } from './dto/category-query.dto';
import { CreateCategoryDto } from './dto/create-category.dto';

@ApiTags('账单类别')
@Controller('category')
export class CategoryController {
  constructor(private readonly categoryService: CategoryService) {}

  // 查询所有类别
  @Get('findAll')
  @Public()
  @ApiOperation({ summary: '查询所有账单类别' })
  findAll(@Query() query: CategoryQueryDto) {
    return this.categoryService.findAll(query);
  }

  // 类别导入
  @Post('import')
  @Public()
  @ApiOperation({ summary: '账单类别导入' })
  @UseInterceptors(FileInterceptor('file'))
  async import(@UploadedFile() file) {
    return this.categoryService.import(file);
  }

  // 添加类别
  @Post('add')
  @Public()
  @ApiOperation({ summary: '添加账单类别' })
  add(@Body() createCategoryDto: CreateCategoryDto) {
    return this.categoryService.add(createCategoryDto);
  }

  // 修改类别
  @Put('update/:id')
  @Public()
  @ApiOperation({ summary: '修改账单类别' })
  update(
    @Body() createCategoryDto: CreateCategoryDto,
    @Param('id') id: number,
  ) {
    return this.categoryService.update(createCategoryDto, id);
  }

  // 删除类别
  @Delete('delete/:id')
  @Public()
  @ApiOperation({ summary: '删除账单类别' })
  delete(@Param('id') id: number) {
    return this.categoryService.delete(id);
  }
}
