import { Column, Model, Table } from 'sequelize-typescript';

@Table({
  timestamps: false, // 自动维护时间
  tableName: 't_category', // 数据库表名称
  freezeTableName: true, // 禁止修改表名，如果不写会把表名自动复数化
  paranoid: true, // 软删除
})
export class Category extends Model<Category> {
  @Column({ primaryKey: true, autoIncrement: true })
  id: number;

  @Column
  pid: number;

  @Column
  name: string;

  @Column
  icon: string;

  @Column
  flag: string;

  @Column
  sort: number;

  @Column
  create_time: Date;

  @Column
  update_time: Date;
}
