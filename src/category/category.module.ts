import { Module } from '@nestjs/common';
import { CategoryController } from './category.controller';
import { CategoryService } from './category.service';
import { Category } from './category.model';

const CategoryProvider = [
  { provide: 'CategoryRepository', useValue: Category },
];
@Module({
  providers: [CategoryService, ...CategoryProvider],
  exports: [CategoryService],
  controllers: [CategoryController],
})
export class CategoryModule {}
