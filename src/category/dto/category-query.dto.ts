import { ApiProperty } from '@nestjs/swagger';

export class CategoryQueryDto {
  @ApiProperty({ description: '排序字段', example: 'sort' })
  readonly orderBy?: string;

  @ApiProperty({ description: '排序方式', example: 'DESC' })
  readonly sort?: string;

  @ApiProperty({ description: '名称' })
  readonly name?: string;

  @ApiProperty({ description: '类型' })
  readonly flag?: string;
}
