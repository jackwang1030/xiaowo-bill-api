import { IsNotEmpty, MaxLength } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateCategoryDto {
  @ApiProperty({ description: '父id', example: '0' })
  readonly pid: number;

  @ApiProperty({ description: '名称', example: '早餐' })
  @IsNotEmpty({ message: '名称不能为空' })
  @MaxLength(4, { message: '名称不超过4个字符' })
  readonly name: string;

  @ApiProperty({ description: '图标' })
  @IsNotEmpty({ message: '图标不能为空' })
  readonly icon: string;

  @ApiProperty({ description: '标识', example: 'pay' })
  @IsNotEmpty({ message: '标识不能为空' })
  readonly flag: string;

  @ApiProperty({ description: '排序', example: '0' })
  @IsNotEmpty({ message: '排序不能为空' })
  readonly sort: number;
}
