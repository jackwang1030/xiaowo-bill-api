// 根据key返回对应的value数组
export function getArrayProps(arr: any[], key: string): any[] {
  const keys = `${key}`;
  const res = [];
  if (arr.length > 0) {
    arr.forEach((t) => {
      res.push(t[keys]);
    });
  }
  return res;
}

// 数组转tree
export function arrayToTree(list: any[], name = 'children' as string): any[] {
  if (list.length === 1) {
    return list;
  }
  const res = [];
  const map = list.reduce((res, v) => ((res[v.id] = v), res), {});
  for (const item of list) {
    if (item.pid === 0) {
      res.push(item);
      continue;
    }
    if (item.pid in map) {
      const parent = map[item.pid];
      parent[name] = parent[name] || [];
      parent[name].push(item);
    }
  }
  return res;
}
