import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UserModule } from '../user/user.module';
import { PassportModule } from '@nestjs/passport';
import { JwtStrategy } from './jwt.strategy';
import { JwtModule } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { CacheService } from 'src/cache/cache.service';

const jwtModule = JwtModule.registerAsync({
  inject: [ConfigService],
  useFactory: async (configService: ConfigService) => {
    const tokenExpire = configService.get('token_expire');
    const jwt = configService.get('jwt');
    return {
      secret: jwt.secret,
      signOptions: { expiresIn: +tokenExpire.access_token_expire },
    };
  },
});

@Module({
  imports: [
    UserModule,
    PassportModule.register({ defaultStrategy: 'jwt' }),
    jwtModule,
  ],
  providers: [AuthService, JwtStrategy, CacheService],
  exports: [AuthService],
})
export class AuthModule {}
