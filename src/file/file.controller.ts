import {
  Controller,
  Post,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { Public } from 'src/auth/auth.decorator';
import { FileService } from './file.service';

@ApiTags('文件上传')
@Controller('file')
export class FileController {
  constructor(private readonly fileService: FileService) {}
  @ApiOperation({ summary: '文件上传接口' })
  @Public()
  @Post('upload')
  @UseInterceptors(FileInterceptor('file'))
  async UploadedFile(@UploadedFile() file) {
    return this.fileService.create(file);
  }
}
