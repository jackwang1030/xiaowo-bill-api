import { Inject, Injectable } from '@nestjs/common';
import { File } from './file.model';

@Injectable()
export class FileService {
  constructor(
    @Inject('FileRepository')
    private readonly fileRepository: typeof File,
  ) {}
  async create(data: any) {
    const file = new File();
    file.path = data.path;
    file.filename = data.originalname.split('.')[0];
    file.size = data.size;
    file.extend = data.originalname.split('.').pop();
    file.create_time = new Date();
    const result = await file.save();
    return {
      id: result.id,
      path: result.path,
      filename: result.filename,
      size: result.size,
      extend: result.extend,
    };
  }
}
