import { Column, Model, Table } from 'sequelize-typescript';

@Table({
  timestamps: false, // 自动维护时间
  tableName: 't_file', // 数据库表名称
  freezeTableName: true, // 禁止修改表名，如果不写会把表名自动复数化，users
  paranoid: false, // 软删除
})
export class File extends Model<File> {
  @Column({ primaryKey: true, autoIncrement: true })
  id: number;

  @Column
  filename: string;

  @Column
  extend: string;

  @Column
  size: string;

  @Column
  path: string;

  @Column
  create_time: Date;
}
