import { Module } from '@nestjs/common';
import { FileController } from './file.controller';
import { FileService } from './file.service';
import { MulterModule } from '@nestjs/platform-express';
import dayjs = require('dayjs');
import { diskStorage } from 'multer';
// import * as nuid from 'nuid';
import { File } from './file.model';

const fileProvider = [{ provide: 'FileRepository', useValue: File }];

@Module({
  imports: [
    MulterModule.register({
      storage: diskStorage({
        //自定义路径
        destination: `./upload/${dayjs().format('YYYY-MM-DD')}`,
        filename: (req, file, cb) => {
          // 自定义文件名
          // let filename = '';
          // if (file.mimetype === 'application/vnd.ms-excel') {
          //   filename = `${nuid.next()}.xls`;
          // } else if (
          //   file.mimetype ===
          //   'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
          // ) {
          //   filename = `${nuid.next()}.xlsx`;
          // } else {
          //   filename = `${nuid.next()}.${file.originalname.split('.').pop()}`;
          // }
          // const filename = `${nuid.next()}.${file.originalname}`;
          //  return cb(null, filename);
          return cb(null, file.originalname);
        },
      }),
    }),
  ],
  controllers: [FileController],
  providers: [FileService, ...fileProvider],
})
export class FileModule {}
