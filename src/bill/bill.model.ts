import { Column, Model, Table } from 'sequelize-typescript';

@Table({
  timestamps: false, // 自动维护时间
  tableName: 't_bill', // 数据库表名称
  freezeTableName: true, // 禁止修改表名，如果不写会把表名自动复数化
  paranoid: true, // 软删除
})
export class Bill extends Model<Bill> {
  @Column({ primaryKey: true, autoIncrement: true })
  id: number;

  @Column
  user_id: number;

  @Column
  category_id: number;

  @Column
  bill_img: string;

  @Column
  remark: string;

  @Column
  cost: number;

  @Column
  pay_method: number;

  @Column
  flag: number;

  @Column
  week: number;

  @Column
  year: number;

  @Column
  month: number;

  @Column
  day: number;

  @Column
  record_time: Date;

  @Column
  create_time: Date;

  @Column
  update_time: Date;
}
