import { Module } from '@nestjs/common';
import { BillController } from './bill.controller';
import { BillService } from './bill.service';
import { Bill } from './bill.model';

const BillProvider = [{ provide: 'BillRepository', useValue: Bill }];

@Module({
  providers: [BillService, ...BillProvider],
  exports: [BillService],
  controllers: [BillController],
})
export class BillModule {}
