import { ApiProperty } from '@nestjs/swagger';

export class BillQueryDto {
  @ApiProperty({ description: '页码', example: 1 })
  readonly pageNo: number;

  @ApiProperty({ description: '页数', example: 10 })
  readonly pageSize: number;

  @ApiProperty({ description: '排序字段', example: 'create_time' })
  readonly orderBy?: string;

  @ApiProperty({ description: '排序方式', example: 'DESC' })
  readonly sort?: string;

  @ApiProperty({ description: '时间' })
  readonly billDate?: Date;

  @ApiProperty({ description: '类型' })
  readonly category?: number;
}
