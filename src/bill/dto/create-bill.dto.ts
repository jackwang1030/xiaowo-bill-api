import { IsEnum, IsNotEmpty, MaxLength } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateBillDto {
  @ApiProperty({ description: '分类' })
  @IsNotEmpty({ message: '分类不能为空' })
  readonly category_id: number;

  @ApiProperty({ description: '备注' })
  @MaxLength(255, { message: '备注不超过255个字符' })
  readonly remark: string;

  @ApiProperty({ description: '费用' })
  @IsNotEmpty({ message: '费用不能为空' })
  readonly cost: number;

  @ApiProperty({ description: '账单日期' })
  @IsNotEmpty({ message: '账单日期不能为空' })
  readonly record_time: string;

  @ApiProperty({ description: '照片' })
  readonly bill_img?: string;

  @ApiProperty({
    description: '支付方式',
    example: '支付方式 1：支付宝，2：微信，3：银行卡 4：现金',
  })
  @IsNotEmpty({ message: '支付方式不能为空' })
  readonly pay_method: number;

  @ApiProperty({ description: '类型', example: '类型：pay=支出，income=收入' })
  @IsNotEmpty({ message: '类型不能为空' })
  @IsEnum(['pay', 'income'], {
    message: '类型必须是：pay=支出，income=收入',
  })
  readonly flag: string;
}
