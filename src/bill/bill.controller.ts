import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  Req,
} from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { BillService } from './bill.service';
import { BillQueryDto } from './dto/bill-query.dto';
import { CreateBillDto } from './dto/create-bill.dto';

export class AnalyzeQuery {
  readonly billDate: string;
}
@ApiTags('账单')
@Controller('bill')
export class BillController {
  constructor(private readonly billService: BillService) {}

  // 查询所有账单
  @ApiOperation({ summary: '查询所有账单' })
  @Get('findAll')
  findAll(@Req() req, @Query() query: BillQueryDto) {
    return this.billService.findAll(query, req.headers.user);
  }

  // 添加账单
  @ApiOperation({ summary: '添加账单' })
  @Post('add')
  add(@Req() req, @Body() body: CreateBillDto) {
    return this.billService.add(body, req.headers.user);
  }

  // 修改账单
  @ApiOperation({ summary: '修改账单' })
  @Put('update/:id')
  update(@Param('id') id: number, @Body() body: CreateBillDto) {
    return this.billService.update(id, body);
  }

  // 删除账单
  @ApiOperation({ summary: '删除账单' })
  @Delete('delete/:id')
  delete(@Req() req, @Param('id') id: number) {
    return this.billService.delete(id, req.headers.user);
  }

  // 按月查询账单统计数据
  @ApiOperation({ summary: '按月查询账单统计数据' })
  @Get('analyze')
  analyze(@Req() req, @Query() query: AnalyzeQuery) {
    return this.billService.analyze(query, req.headers.user);
  }
}
