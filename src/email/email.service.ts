import { ISendMailOptions, MailerService } from '@nestjs-modules/mailer';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import dayjs = require('dayjs');
import { CacheService } from 'src/cache/cache.service';
import { Logger } from 'src/utils/log4js';

@Injectable()
export class EmailService {
  constructor(
    private readonly mailerService: MailerService,
    private readonly cacheService: CacheService,
  ) {}

  /**
   * 发送邮件验证码
   * @param data 邮件主体信息
   */
  async sendEmailCode(data) {
    try {
      const code = Math.random().toString().slice(-6);
      const date = dayjs().format('YYYY-MM-DD HH:mm:ss'); // 当前时间
      const sendMailOptions: ISendMailOptions = {
        to: data.email,
        subject: data.subject || '用户邮箱验证',
        //这里写你的模板名称，如果你的模板名称的单名如 validate.ejs ,直接写validate即可 系统会自动追加模板的后缀名,如果是多个，那就最好写全。
        template: 'validate.code.ejs',
        //内容部分都是自定义的
        context: {
          code, //验证码
          date, //日期
          sign: data.sign || '系统邮件,回复无效。', //签名
        },
      };
      await this.mailerService
        .sendMail(sendMailOptions)
        .then(() => {
          Logger.info(
            `发送邮件给: ${data.email}, 成功! 主题:${
              data.subject || '用户邮箱验证'
            }`,
          );
        })
        .catch((error) => {
          Logger.error(
            `发送邮件给: ${data.email} 出错! 主题:${
              data.subject || '用户邮箱验证'
            }`,
            error,
          );
        });
      await this.cacheService.set(`email_code_${data.email}`, code, 60 * 30);
      return { msg: `发送邮件给: ${data.email}, 成功!` };
    } catch (error) {
      Logger.error('发送邮件出错:', error);
      throw new UnauthorizedException('发送邮件出错');
    }
  }
}
