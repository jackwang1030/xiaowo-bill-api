import { SendEmailDto } from './dto/send-email.dto';
import { Body, Controller, Post } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { Public } from 'src/auth/auth.decorator';
import { EmailService } from './email.service';

@ApiTags('邮件')
@Controller('email')
export class EmailController {
  constructor(private emailService: EmailService) {}

  @ApiOperation({ summary: '发送邮件接口' })
  @Public()
  @Post('/sendCode')
  async sendEmailCode(@Body() sendEmailDto: SendEmailDto) {
    return this.emailService.sendEmailCode(sendEmailDto);
  }
}
