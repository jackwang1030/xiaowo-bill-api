import { IsString, IsEmail, MaxLength } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class SendEmailDto {
  @ApiProperty({ description: '邮件主题', example: '用户邮箱验证' })
  @MaxLength(20, { message: '邮件主题不超过20个字符' })
  readonly subject: string;

  @ApiProperty({ description: '邮箱', example: '2726653264@qq.com' })
  @IsEmail({}, { message: '邮箱格式不正确' })
  readonly email: string;

  @ApiProperty({ description: '签名', example: '系统邮件,回复无效' })
  @IsString({ message: '签名格式不正确' })
  readonly sign?: string;
}
