import { Controller, Post, Body, Get, Query } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { AuthService } from './auth/auth.service';
import { AppService } from './app.service';
import { Public } from './auth/auth.decorator';
import { UserLoginDto } from './user/dto/user-login.dto';

@ApiTags('公共模块')
@Controller('common')
export class AppController {
  constructor(
    private readonly authService: AuthService,
    private readonly appService: AppService,
  ) {}

  @ApiOperation({ summary: '用户登录' })
  @Public()
  @Post('login')
  login(@Body() userLoginDto: UserLoginDto) {
    return this.authService.login(userLoginDto);
  }

  @ApiOperation({ summary: '获取图片验证码' })
  @Public()
  @Get('getImgVerification')
  getVerification() {
    return this.appService.getVerification();
  }

  @ApiOperation({ summary: '刷新token' })
  @Public()
  @Get('refreshToken')
  refreshToken(@Query('refreshToken') refreshToken: string) {
    return this.appService.refreshToken(refreshToken);
  }
}
