import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import envConfig from '../config/env';
import { DatabaseModule } from './sequelize.module';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';
import { CacheModule } from './cache/cache.module';
import { EmailModule } from './email/email.module';
import { FileModule } from './file/file.module';
import { JwtAuthGuard } from './auth/auth.guard';
import { APP_GUARD } from '@nestjs/core';
import { CacheService } from './cache/cache.service';
import { CategoryModule } from './category/category.module';
import { BillController } from './bill/bill.controller';
import { BillModule } from './bill/bill.module';
import configuration from '../config/configuration';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true, // 设置为全局
      envFilePath: [envConfig.path], // 设置环境配置文件路径
      load: [configuration],
    }),
    DatabaseModule,
    AuthModule,
    CacheModule,
    FileModule,
    UserModule,
    EmailModule,
    CategoryModule,
    BillModule,
  ],
  controllers: [AppController, BillController],
  providers: [
    AppService,
    CacheService,
    {
      provide: APP_GUARD,
      useClass: JwtAuthGuard,
    },
  ],
})
export class AppModule {}
