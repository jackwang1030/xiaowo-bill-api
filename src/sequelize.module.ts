import { Sequelize } from 'sequelize-typescript';
import { Module } from '@nestjs/common';
// import { ConfigService } from '@nestjs/config';
import { File } from './file/file.model';
import { User } from './user/user.model';
import { Bill } from './bill/bill.model';
import { Category } from './category/category.model';

import config from '../config/db';

const {
  mysql: { port, host, user, password, database },
} = config;

export const databaseProviders = [
  {
    provide: 'SequelizeProvider',
    useFactory: async () => {
      const sequelize = new Sequelize({
        dialect: 'mysql',
        host: host,
        port: port,
        username: user,
        password: password,
        database: database,
        timezone: '+08:00',
        dialectOptions: {
          dateStrings: true,
          typeCast: true,
        },
      });
      sequelize.addModels([File, User, Category, Bill]);
      await sequelize.sync();
      return sequelize;
    },
  },
];
@Module({
  providers: [...databaseProviders],
  exports: [...databaseProviders],
})
export class DatabaseModule {}
