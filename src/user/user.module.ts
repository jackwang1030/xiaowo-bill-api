import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { User } from './user.model';
import { UserController } from './user.controller';
import { CacheService } from '../cache/cache.service';

const UserProvider = [{ provide: 'UserRepository', useValue: User }];
@Module({
  providers: [UserService, ...UserProvider, CacheService],
  exports: [UserService],
  controllers: [UserController],
})
export class UserModule {}
