import { Body, Controller, Get, Post, Query, Req } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { Public } from 'src/auth/auth.decorator';
import { CreateUserDto } from './dto/create-user.dto';
import { UserService } from './user.service';

@ApiTags('用户')
@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @ApiOperation({ summary: '用户注册' })
  @Public()
  @Post('create')
  create(@Body() createUserDto: CreateUserDto) {
    return this.userService.create(createUserDto);
  }

  @ApiOperation({ summary: '用户获取个人信息' })
  @Get('getInfo')
  findOne(@Req() req) {
    return {
      data: req.headers.user,
    };
  }

  @ApiOperation({ summary: '根据用户id获取用户信息' })
  @Get('getUserById')
  findUserById(@Query('id') id: string) {
    return this.userService.findUserById(id);
  }

  @ApiOperation({ summary: '退出登录' })
  @Public()
  @Post('logout')
  logout(@Req() req) {
    return this.userService.logout(req);
  }
}
