import { IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class UserLoginDto {
  @ApiProperty({ description: '账号' })
  @IsNotEmpty({ message: '账号不能为空' })
  readonly username: string;

  @ApiProperty({ description: '密码' })
  @IsNotEmpty({ message: '密码不能为空' })
  readonly password: string;

  @ApiProperty({ description: '验证码' })
  readonly code?: string;

  @ApiProperty({ description: 'nuid' })
  readonly nuid?: string;
}
