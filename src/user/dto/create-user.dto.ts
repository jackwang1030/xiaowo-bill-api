import {
  IsString,
  IsEmail,
  MinLength,
  IsNotEmpty,
  MaxLength,
  IsNumber,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateUserDto {
  @ApiProperty({ description: '手机号', example: '15188211507' })
  readonly phone: string;

  @ApiProperty({ description: '用户名', example: 'wmt' })
  @IsString()
  @IsNotEmpty({ message: '用户名不能为空' })
  @MaxLength(20, { message: '用户名不超过20个字符' })
  readonly username: string;

  @ApiProperty({ description: '邮箱', example: '123456@qq.com' })
  @IsEmail()
  readonly email: string;

  @ApiProperty({ description: '密码', example: '123456' })
  @IsString()
  @MinLength(6, { message: '密码不能少于6个字符' })
  readonly password: string;

  @ApiProperty({ description: '头像', example: 'https://www.baidu.com' })
  @IsString({ message: '头像地址格式不正确' })
  readonly avatar: string;

  @ApiProperty({ description: '性别', example: '1: 男, 2: 女, 3: 保密' })
  @IsNumber({}, { message: '性别只能是数字' })
  readonly sex: number;
}
