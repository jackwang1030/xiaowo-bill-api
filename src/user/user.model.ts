import { BeforeCreate, Column, Model, Table } from 'sequelize-typescript';
import * as bcrypt from 'bcryptjs';

@Table({
  timestamps: false, // 自动维护时间
  tableName: 't_user', // 数据库表名称
  freezeTableName: true, // 禁止修改表名，如果不写会把表名自动复数化，users
  paranoid: true, // 软删除
})
export class User extends Model<User> {
  @Column({ primaryKey: true, autoIncrement: true })
  id: number;

  @Column
  username: string;

  @Column
  nickname: string;

  @Column
  phone: string;

  @Column
  email: string;

  @Column
  password: string;

  @Column
  avatar: string;

  @Column
  sex: number;

  @Column
  bill_day: number;

  @Column
  bill_num: number;

  @Column
  budget: number;

  @Column
  create_time: Date;

  @Column
  update_time: Date;

  @BeforeCreate
  static encryptPwd(instance: User) {
    if (!instance.password) return;
    instance.password = bcrypt.hashSync(instance.password, 10);
  }
}
