// PM2 环境变量隔离Node.js项目的开发与生产环境 服务器运行
// NODE_ENV=production pm2 start main.js --name="xiaowo-server" --update-env
module.exports = {
  apps: [
    {
      name: 'app1',
      script: './app.js',
      env: {
        NODE_ENV: 'development',
      },
      env_production: {
        NODE_ENV: 'production',
      },
    },
  ],
};
